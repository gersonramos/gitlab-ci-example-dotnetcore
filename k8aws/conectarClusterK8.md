# CONECTANDO NO CLUSTER DO K8

## Resumo

Conectar no cluster para realizar analise dos serviços que forem implantados no cluster, etc.

### Pré-requisitos
**PIP-PYTHON**/
**AWS CLI**/
**AWS-IAM-AUTHENTICATOR**/
**KUBECTL**


### Procedimento

Para usar o kubectl com seu clusters do Amazon EKS, você precisa instalar um binário que pode criar o token de segurança do cliente para comunicação com o servidor da API do cluster. O comando aws eks get-token, disponível na versão 1.16.156 ou superior da AWS CLI, suporta a criação do token de segurança.
Para instalar ou atualizar o AWS CLI no LINUX:

* [AWSCLI+PIP](https://docs.aws.amazon.com/pt_br/cli/latest/userguide/install-linux.html)
* [AWS-IAM-AUTHENTICATOR](https://docs.aws.amazon.com/pt_br/eks/latest/userguide/install-aws-iam-authenticator.html)

Se você já tem o pip e uma versão compatível do Python, é possível instalar o CLI da AWS com o seguinte comando:
```
pip install awscli --upgrade –user
```
A versão Python do seu sistema precisa ser 2.7.9 ou superior. Caso contrário, você receberá erros de ‘hostname doesn't match’
Para obter mais informações, consulte:
* [hostname doesn't match](https://2.python-requests.org//en/master/community/faq/#what-are-hostname-doesn-t-match-errors)

# Configurar credenciais do AWS CLI

AWS CLI exige que você tenha credenciais da AWS configuradas no seu ambiente. O comando aws configure é a maneira mais rápida de configurar sua instalação da AWS CLI para uso geral.
Exemplo:
```
aws configure

Será pedido as seguintes informações:
AWS Access Key ID [None]: ACESSKEYAWS
AWS Secret Access Key [None]: SECRETACESSKEYAWS
Default region name [None]: us-east-2 (EXEMPLO DE REGIAO)
Default output format [None]: json (EXEMPLO DE OUTPUT)

```
# Instalar e configurar o kubectl para o Amazon EKS

Você tem várias opções para baixar e instalar kubectl para o seu sistema operacional. 
O binário kubectl está disponível em muitos gerenciadores de pacotes de sistema operacional, e essa opção geralmente é muito mais fácil do que o processo de download e instalação manual. Você pode seguir as instruções para seu sistema operacional ou gerenciadores de pacotes específicos na Documentação do Kubernetes para instalar.
* [Documentação do K8](https://kubernetes.io/docs/tasks/tools/install-kubectl/)

O Amazon EKS também fornece binários kubectl que você pode usar que são idênticos aos binários kubectl upstream com a mesma versão. Para instalar o binário fornecido pelo Amazon EKS para o seu sistema operacional.

* [KUBECTL](https://docs.aws.amazon.com/pt_br/eks/latest/userguide/install-kubectl.html)

Antes de verificar se o kubectl está conectado ao cluster é necessario aplicar a seguinte configuração:
```
aws eks --region REGIAOAWSDOCLUSTER update-kubeconfig --name NOMEDOCLUSTER
```

Para verificar se o kubectl está conectado ao cluster execute o seguinte comando:
```
kubectl get svc
```
O serviço do cluster k8 será exibido.
Exemplo:
```
NAME             TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)   AGE
svc/kubernetes   ClusterIP   10.100.0.1   <none>        443/TCP   1m

```

Se você receber qualquer outro erro de autorização ou de tipo de recurso consulte:
na seção de solução de problemas.
* [Não autorizado ou acesso negado (kubectl)](https://docs.aws.amazon.com/pt_br/eks/latest/userguide/troubleshooting.html#unauthorized) - Solução de problemas
