# Comandos Uteis K8

## Resumo

Alguns comandos uteis que podem ser executados no cluster.

### Pré-requisitos

**kubectl**

### Procedimento

Criação de namespace:

```
kubectl create namespace NAMESPACEESCOLHIDO
```
OBS: podemos usar -n ou também -namespace para a execução do comando acima.

Criação de secret:
```
kubectl create secret XXXX
```
Verificar um secret ou todos:
```
kubectl get secrets OU kubectl get secret NOMEDOSECRET --namespace=NOMEDONAMESPACE
```

Quando criamos um SECRET para um projeto especifico do gitlab, é necessario realizar a criação de forma diferente.

Exemplo:
```
kubectl create secret docker-registry testelucao --docker-server="registry.gitlab.com" --docker-username="ir até settings>>repository>>DeployTokens" --docker-password="PegarNoMesmoLugarDoUsername" --docker-email="xxxx@bancosemear.com.br"
```
**Qualquer dúvida referente ao item acima estou a disposição**

Quando um pod fica travado com o status unknown é necessario forçar sua finalização:
```
kubectl delete pod NOMEDOPOD --namespace=NOMEDONAMESPACECASONECESSARIO --grace-period=0 --force
```
Quando for necessário editar um serviço especifico:
```
kubectl -n NOMEDONAMESPACE edit svc NOMEDOSERVICO
```
OBS: podemos usar svc ou também services para a execução do comando acima.

Quando é necessario pegar log de um pod:

```
kubectl logs NOMEDOPOD -p
```
OBS: Caso necessário passar o namespace.

Quando é necessario pegar o deployment:

```
kubectl get deployments OU kubectl get deployment --namespace=NOMEDONAMESPACE
```
OBS: Caso necessário passar o namespace.

Quando é necessario pegar inf sobre  o serviço:
```
kubectl get svc OU kubectl get svc --namespace=NOMEDONAMESPACE
```
### TODOS OS COMANDOS ACIMA PODEM SER REALIZADOS VIA INTERFACE GRAFICA PELO CONSOLE DASHBOARD DO CLUSTER
