# CONECTANDO NO CONTAINER MICROSERVIÇO K8

## Resumo

Para se conectar no container do microserviço para poder avaliar/debugar a aplicação é necessário que o kubectl esteja instalado e configurado para se comunicar com o cluster.

### Pré-requisitos

**kubectl**

### Procedimento

Execute o seguinte comando em seu utilitario de linha de comando:

```
kubectl describe pods
```

Uma tela parecida com essa será exibida:
![](Imagens/ContainerMS1.png)


Anote o nome do POD conforme imagem abaixo:

![](Imagens/ContainerMS2.png)

Vá até seu utilitário de linha de comando e execute:

```
kubectl exec -it --namespace NOMEDONAMESPACE NOMEDOPOD bash
```

Note que no nosso caso o namespace está como default então não é necessário especificar o namespace:

![](Imagens/ContainerMS3.png)

O comando fica da seguinte maneira:

```
kubectl exec -it deploy-devlabs-account-6f9fcb8bfb-q97b9 bash
```
Após executar o comando podemos navegar pelo container com os comandos do bash:

![](Imagens/ContainerMS4.png)
